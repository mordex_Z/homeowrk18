﻿#include <iostream>

class Stack
{
private:
    int* array;
    int size;
    int full;
public:
    Stack (int x)
    {
        array = new int[x];
        size = x;
        full = 0;
    }


    void push(int a)
    {

        if (full >= size)

        {
            size += 1;
            int* newarray = new int[size];
            for (int i = 0; i < size - 1; i++)
            {
                newarray[i] = array[i];
            }
            delete[] array;
            newarray[full] = a;
            array = newarray;
            newarray = nullptr;
        }

        else
        {
            array[full] = a;
        }

        full += 1;
    }

    int pop()
    {
        if (full > 0)
        {
            full -= 1;
            return array[full];
        }
    }
    void print()
    {
        std::cout << "\n";
        for (int i = 0; i < size; i++)
        {
            std::cout << array[i];
        }
        std::cout << "\n";
    }
};

int main()
{
    int x;
    std::cin >> x;
    Stack some(x);
    some.push(2);
    some.push(6);
    some.push(1);
    std::cout << some.pop();
    some.push(6);
    some.push(7);
    some.print();
}